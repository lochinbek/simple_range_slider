import RangeSlider from "@/RangeSlider";

const plugin = {
    install(Vue) {
        Vue.component('SimpleRangeSlider', RangeSlider)
    }
}

if (window.Vue) {
    plugin.install(window.Vue)
}

export default plugin;

