# clean_vue2

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


```
// for import
import SimpleRangeSlider from 'simple-range-slider-real-soft'
import 'simple-range-slider-real-soft/dist/range-slider.css'
```

```
// for use
 <SimpleRangeSlider />
```

Available props:

* name - name of the slider input.
* value - current value of the slider.
* disabled - if true, the slider value cannot be updated.
* min - minimum value of the slider.
* max - maximum value of the slider.
* step - granularity of the slider value. e.g. if this is 3, the slider value will be 3, 6, 9, ...



Available slots:

* knob - slot for replacing knob